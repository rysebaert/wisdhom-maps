# wisdhom-maps

A [website](https://rysebaert.gitpages.huma-num.fr/wisdhom-maps/index.html) displaying maps created within the WIsDHoM project (Wealth Inequalities and the Dynamics of the Housing Market)
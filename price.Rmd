---
title: "Prix au m²"
output: 
  html_document: 
    number_sections: no
    toc: no
editor_options: 
  chunk_output_type: console
---


```{r setup, echo=FALSE, cache=FALSE, warning=FALSE}
library(knitr)
library(rmdformats)
library(slickR)

## Global options
options(max.print="100")
opts_chunk$set(echo=TRUE,
               cache=TRUE,
               prompt=FALSE,
               tidy=TRUE,
               comment=NA,
               message=FALSE,
               warning=FALSE, 
               eval=TRUE)
opts_knit$set(width=100)
```


L'analyse porte ici sur l'évolution des prix moyens au m² par type de bien (maisons ou appartements). Ces prix sont calculés en réalisant la moyenne des prix au m² observés par commune d'appartenance et par année.

Les communes enregistrant moins de 5 transactions par année ne sont pas représentées (seuil de confidentialité, robustesse des résultats).

La rupture de couleur des représentations cartographiques correspond à la médiane des valeurs renseignée sur l'ensemble de la période analysée à la commune (1998 - 2018). La discrétisation suivant la méthode des quantiles. 

Pour les analyses croisées, les données pour l'Ile-de-France correspondent aux transactions opérées en 1996 pour 1998 ; et celles de 2003 correspondent à 2002. 

Les représentations au carreau (1 km - seuls les carreaux peuplés sont représentés) présentent les prix moyens dans un voisinage géographique de 2km. Ce lissage spatial permet de s'affranchir de la maille territoriale et ainsi détecter les configurations spatiales essentielles qui structurent cet espace. 


#   {.tabset}


## Paris

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_A_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br>
<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_A_PRIX_M2.zip)
</center><br>

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_M_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_M_PRIX_M2.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_A_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_A_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_M_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_M_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_A_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_A_PRIX_MOY_POT.zip)
</center><br>



```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/paris/PARIS_M_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<center>
[Télécharger les .png associés (.zip)](docs/paris/PARIS_M_PRIX_MOY_POT.zip)
</center>


## Lyon

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/lyon/LYON_A_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_A_PRIX_M2.zip)
</center><br>

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/lyon/LYON_M_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_M_PRIX_M2.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/lyon/LYON_A_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_A_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/lyon/LYON_M_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_M_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/lyon/LYON_A_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_A_PRIX_MOY_POT.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/LYON/LYON_M_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/lyon/LYON_M_PRIX_MOY_POT.zip)
</center>



## Avignon

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_A_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_A_PRIX_M2.zip)
</center><br>

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_M_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_M_PRIX_M2.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_A_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_A_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_M_IQR/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_M_IQR.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_A_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_A_PRIX_MOY_POT.zip)
</center><br>


```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/avignon/AVIGNON_M_PRIX_MOY_POT/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/avignon/AVIGNON_M_PRIX_MOY_POT.zip)
</center>



## Analyses croisées

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/cross_analysis/CA_A_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```

<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/cross_analysis/CA_A_PRIX_M2.zip)
</center><br>

```{r, eval = TRUE, echo = FALSE, warning= FALSE}
dir <- "docs/cross_analysis/CA_M_PRIX_M2/"
files <- paste0(dir, list.files(dir))
slickR(obj = files, height = 500, width = "100%")  + 
  settings(dots = TRUE, autoplay = TRUE, fade = TRUE)
```
<br><br>
<center>
[Télécharger les .png associés (.zip)](docs/cross_analysis/CA_M_PRIX_M2.zip)
</center><br>


## Reproductibilité

Code R qui génère les indicateurs harmonisés (à la commune ou sur carreau de grille lissé) depuis le tableau de données consolidé (granularité = transaction).

### Prix au m² (commune)

<details>
  <summary>Voir le code</summary>
```{r, eval = FALSE}
library(sf)

# Import des couches de transaction (appartement ou maison)
# et du fond de carte communal (non diffusable)
paris <- getLayers(x = "Paris")
pt <- parPA
com <- paris$com

# Calcul indicateur
# Intersection espace d'étude
pt$PRIX_M2 <- pt$prix / pt$srf_hab
pt <- st_intersection(com, pt)
pt <- st_set_geometry(pt, NULL)

# Toutes les années disponibles dans le fichier
years <- levels(as.factor(pt$annee))

# Fichier de sortie (code et nom de commune seul)
comdf <- com[,c("INSEE_COM", "NOM_COM"), drop = TRUE]

# Nombre de transaction par commune et par année
for (i in 1:length(years)){
  tmp <- pt[pt$annee == years[i],]
  tmp <- aggregate(tmp$INSEE_COM, by = list(tmp$INSEE_COM), FUN = length)
  colnames(tmp) <- c("INSEE_COM", paste0("N_", years[i]))
  comdf <- merge(comdf, tmp, by = "INSEE_COM", all.x = TRUE)
}

# Prix moyen au m²
for (i in 1:length(years)){
  tmp <- pt[pt$annee == years[i],]
  tmp <- aggregate(tmp$PRIX_M2, by = list(tmp$INSEE_COM), FUN = mean)
  colnames(tmp) <- c("INSEE_COM", paste0("PRIX_M2_", years[i]))
  comdf <- merge(comdf, tmp, by = "INSEE_COM", all.x = TRUE)
}
  
# Seuil de confidentialité : si moins de 5 transactions, NA
for (i in 3:length(years)){
  comdf[,i+14] <- ifelse(comdf[,i] >= 5, comdf[,i+14], NA)
}
```
</details> 

### Intervalle inter-quartile

<details>
  <summary>Voir le code</summary>
```{r, eval = FALSE}
library(sf)

# Import des couches de transaction (appartement ou maison)
# et du fond de carte communal (non diffusable)
paris <- getLayers(x = "Paris")
pt <- parPA
com <- paris$com

# Calcul indicateur
# Intersection espace d'étude
pt$PRIX_M2 <- pt$prix / pt$srf_hab
pt <- st_intersection(com, pt)
pt <- st_set_geometry(pt, NULL)

# Toutes les années disponibles dans le fichier
years <- levels(as.factor(pt$annee))

# Fichier de sortie (code et nom de commune seul)
comdf <- com[,c("INSEE_COM", "NOM_COM"), drop = TRUE]

# Agrégation nombre de transactions observées (communes)
for (i in years){
  tmp <- pt[pt$annee == i,]
  tmp <- aggregate(tmp$INSEE_COM, by = list(tmp$INSEE_COM), FUN = length)
  colnames(tmp) <- c("INSEE_COM", paste0("N_", years[i]))
  comdf <- merge(comdf, tmp, by = "INSEE_COM", all.x = TRUE)
}

# Calcul 1er quartile des prix
for (i in years){
  tmp <- pt[pt$annee == i,]
  tmp <- aggregate(tmp$PRIX_M2, by = list(tmp$INSEE_COM), FUN = function(i) quantile(i, probs = 0.25, na.rm = T))
  colnames(tmp) <- c("INSEE_COM", paste0("PRIX_M2_Q25_", i))
  comdf <- merge(comdf, tmp, by = "INSEE_COM", all.x = TRUE)
}

# Calcul 3e quartile des prix
for (i in years){
  tmp <- pt[pt$annee == i,]
  tmp <- aggregate(tmp$PRIX_M2, by = list(tmp$INSEE_COM), FUN = function(i) quantile(i, probs = 0.75, na.rm = T))
  colnames(tmp) <- c("INSEE_COM", paste0("PRIX_M2_Q75_", i))
  comdf <- merge(comdf, tmp, by = "INSEE_COM", all.x = TRUE)
}

# Seuil confidentialité 
# Gestion seuil confidentialité
for (i in 3:16){
  comdf[,i+14] <- ifelse(comdf[,i] >= 5, comdf[,i+14], NA)
  comdf[,i+28] <- ifelse(comdf[,i] >= 5, comdf[,i+28], NA)
}

# Intervalle interquartile
for (i in years){
    comdf[, paste0("IQR_", i)] <- comdf[paste0("PRIX_M2_Q75_", i)]] -
  [[comdf("PRIX_M2_Q25_",  i)]]
}
```
</details> 

### Calcul de potentiel

<details>
  <summary>Voir le code</summary>
```{r, eval = FALSE}
library(mapsf)
library(sf)
library(potential)

# Import des couches de transaction (appartement ou maison)
# et du fond de carte communal (non diffusable)
layers <- getLayers(x = "Paris")
pt <- parPM
com <- layers$com
grid1k <- layers$grid1k

# Sélection des carreaux habités
grid1k <- grid1k[grid1k$Ind>=1, ]

## Maison ----
PM <- st_read("data/conso/paris_points.gpkg", layer = "maisons")
years <- c("2008", "2009","2010","2011","2012","2015", "2018")

# Sélection des transaction a partir de 2008
PM <- PM[PM$annee %in% years,]
PM <- rbind(PM[PM$N == 1, ], 
            PM[PM$N == 2, ], 
            PM[PM$N == 2, ])

# Nombre de transaction maison sur la période
grid1k$n_maison <- sapply(st_intersects(grid1k, PM), length)

# Sélection des carreaux avec >=1 transaction sur la période
target <- grid1k[grid1k$n_maison>=1, ]

### Calcul des potentiels prix, surface et dette ----
st_geometry(target) <- st_centroid(st_geometry(target))
for (i in years){
  pt <- PM[PM$annee == i, ]
  pot <- mcpotential(
    x = pt, 
    y = target, 
    var = c("prix", "srf_hab", "mnt_credit"),
    fun = "e", 
    span = 3000, 
    beta = 2, 
    limit = 6000
  )
  var1 <- paste0("M_PRIX_POT_", i)
  var2 <- paste0("M_SURF_POT_", i)
  var3 <- paste0("M_DEBT_POT_", i)
  res <- data.frame(
    Id_carr1km = target$Id_carr1km, 
    x = pot[, 1], 
    y = pot[, 2], 
    z = pot[, 3]
  )
  colnames(res)[2] <- var1
  colnames(res)[3] <- var2
  colnames(res)[4] <- var3
  grid1k <- merge(grid1k, res, by  = "Id_carr1km", all.x = TRUE)
}

grid <- grid1k

# Prix au m² (potentiel)
for (i in years){
  grid[,paste0("A_PRIX_MOY_POT_", i)] <- 
    grid[[paste0("A_PRIX_POT_", i)]] / 
    grid[[paste0("A_SURF_POT_", i)]]
}
```
</details> 